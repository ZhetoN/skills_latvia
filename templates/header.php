<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/assets/css/admin.css" type="text/css">
</head>
<body>
    <?php
      $menuitems = array(
        array(
            'name' => 'Skolas',
            'pattern' => '/.*\/?school\/?.*/',
            'url' => 'school_list'
        ),
        array(
            'name' => 'Studenti',
            'pattern' => '/.*\/?student\/?.*/',
            'url' => 'student_list'
        ),
        array(
            'name' => 'Nozares',
            'pattern' => '/.*\/?occupation\/?.*/',
            'url' => 'occupation_list'
        ),
        array(
            'name' => 'Profesijas',
            'pattern' => '/.*\/?career\/?.*/',
            'url' => 'career_list'
        ),
    );

    $menu = array();
    foreach ($menuitems as $item) {
      if (preg_match($item['pattern'], $this->getCurrentRoute())) {
        $menu[] = '<li class="active">';
        if ($item['url'] == $this->getCurrentRoute()) {
          $menu[] = "<a>{$item['name']}<span class=\"sr-only\">(current)</span></a>";
        } else {
          $menu[] = "<a href=\"{$this->urlFor($item['url'])}\">{$item['name']}<span class=\"sr-only\">(current)</span></a>";
        }
        $menu[] = "</li>";
      } else {
        $menu[] = "<li><a href=\"{$this->urlFor($item['url'])}\">{$item['name']}</a></li>";
      }
    }
    $menu = implode($menu);
    ?>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">SkillsLatvia</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right hidden-sm hidden-md hidden-lg">
            <?= $menu ?>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <?= $menu ?>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">