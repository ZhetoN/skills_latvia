<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Skills</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css" />
  </head>
  <body>
    <div class="app__wrapper">
      <header class="app__header">
        <div clas="app__top-bar">
          <div class="left"><a href="#"><img src="/assets/images/logo-1.png" alt="" /></a></div>
          <div class="right"><a href="#"><img src="/assets/images/logo-2.png" alt="" /></a></div>
          <div class="clearer"></div>
        </div>
        <div class="app__menu-bar">
          <ul class="app__menu-top">
            <li class="menu-top__proffessions"><a href="#">Profesijas un Dalībnieki</a></li>
            <li class="menu-schools"><a href="#">Pārstāvētās skolas</a></li>
          </ul>
          <div class="clearer"></div>
        </div>
      </header>
      <div class="app__content_proffesions">
        <div class="app__content_wrapper">
        <?php $numerals = array('first', 'second', 'third'); ?>
        <?php foreach ($occupations as $occupation): ?>
          <div class="app__content_row">
            <div class="app__content_row_opener">
              <a href="#">
                <?php if ($occupation->icon): ?>
                  <img src="/assets/images/<?= $occupation->icon()->first()->filename ?>" alt="" />
                <?php else: ?>
                  <span class="intro-ikonas skills-skills_ikonas-06"></span>
                <?php endif ?>
                <span><?= $occupation->name ?></span>
                <div class="clearer"></div>
              </a>
            </div>
            <div class="app__content_row_data_content" style="display:none;">
              <div class="app__content_row_data_content_wrapper">
              <?php foreach ($occupation->careers as $career): ?>
                  <?php
                  // $students = $career->students->first()->toArray();
                  // var_dump($students['image']['filename']);
                  ?>
                <div class="app_content_subrow_opener">
                  <span><?= $career->name ?></span>
                  <div class="opener-button"><span class="icon-pluser icon-skills_ikonas_pluss-04"></span></div>
                  <div class="clearer"></div>
                </div>
                <div class="app_content_subrow_content" style="display:none;">
                  <?php $students = $career->students->all(); ?>
                  <?php foreach (array_chunk($students, 3) as $row): ?>
                    <div class="app_content_subrow_row">
                      <?php foreach ($row as $key => $student): ?>
                      <?php $column = $numerals[$key % 3]; ?>
                      <div class="app_content_subrow_column <?= $column ?>-column">
                        <?php $image = $student->image()->first(); ?>
                        <?php if ($image): ?>
                          <img src="images/<?= $image->filename ?>" alt="<?= $student['name'] ?>">
                        <?php else: ?>
                          <img src="images/noimage.jpg" alt="noimage">
                        <?php endif ?>
                        <div class="overlay"></div>
                        <div class="overlay-content">
                          <h4 class="name"><?= $student->name ?></h4>
                          <?php if ($student->birthdate): ?>
                          <?php
                            $birthdate = DateTime::createFromFormat('Y-m-d', $student->birthdate);
                            $today = new DateTime();
                            $interval = $today->diff($birthdate);
                            $age = $interval->format('%y');
                            $sigular = substr($age, -1) === '1';
                          ?>
                          <span class="age"><?= $age ?>&nbsp;<?= $sigular ? 'gads' : 'gadi' ?></span>
                          <?php else: ?>
                          <span class="age">&nbsp;</span>
                          <?php endif ?>
                          <p><?= $student->school()->first()->name ?></p>
                        </div>
                      </div>
                      <?php endforeach ?>
                    </div>
                    <div class="clearer"></div>
                  <?php endforeach ?>
                </div>
              <?php endforeach ?>
              </div>
            </div>
          </div>
        <?php endforeach ?>
        </div>
      </div>
      <div class="app__content_schools" style="display: none;">
        <div class="app__content_wrapper">
          <?php foreach ($regions as $region): ?>
          <div class="app__content_row">
            <div class="app__content_row_opener">
              <a href="#">
                <span><?= $region->name ?></span>
                <div class="clearer"></div>
              </a>
            </div>
            <div class="app__content_row_data_content">
              <?php foreach ($region->schools as $school): ?>
              <div class="app_content_subrow_opener">
                <span><?= $school->name ?></span>
                <div class="opener-button"><span class="icon-pluser icon-skills_ikonas_pluss-04"></span></div>
                <div class="clearer"></div>
              </div>
              <div class="app_content_subrow_content">
                <div class="app_content_subrow_row">
                  <div class="app_content_subrow_column">
                    <?php if ($school->image): ?>
                    <img src="/images/<?= $school->image()->first()->filename ?>" style="width:100%;" />
                    <?php endif ?>
                    <p>Konkursanti: <span class="concursant-count"><?= $school->students()->count() ?></span></p>
                    <?php if ($school->url): ?>
                      <p class="more-about-school">Vairāk par skolu: <a target="_blank" href="<?= $school->url ?>"><?= $school->url ?></a></p>
                    <?php endif ?>
                  </div>
                  <div class="clearer"></div>
                </div>
              </div>
              <?php endforeach ?>
            </div>
          </div>
          <?php endforeach ?>
        </div>
      </div>
      <footer class="app__footer"></footer>
    </div>
    <script src="/assets/js/bundle.js"></script>
  </body>
</html>


<!--
          <div class="app__content_row">
            <div class="app__content_row_opener">
              <a href="#">
                <img src="/assets/images/icon-1.png" alt="" />
                <span>Informācijas un komunikāciju tehnoloģijas</span>
                <div class="clearer"></div>
              </a>
            </div>
            <div class="app__content_row_data_content" style="display:none;">
              <div class="app__content_row_data_content_wrapper">
                <div class="app_content_subrow_opener">
                  <span>Web lapu izstrādātājs</span>
                  <div class="opener-button"><span class="icon-pluser icon-skills_ikonas_pluss-04"></span></div>
                  <div class="clearer"></div>
                </div>
                <div class="app_content_subrow_content" style="display:none;">
                  <div class="app_content_subrow_row">
                    <div class="app_content_subrow_column first-column">
                      <img src="images/noimage.jpg" alt="">
                      <div class="overlay"></div>
                      <div class="overlay-content">
                        <h4 class="name">Artis Laizāns</h4>
                        <span class="age">&nbsp;</span>
                        <p>PIKC Rēzeknes tehnikums</p>
                      </div>
                    </div>
                    <div class="app_content_subrow_column second-column">
                      <img src="images/noimage.jpg" alt="">
                      <div class="overlay"></div>
                      <div class="overlay-content">
                        <h4 class="name">Aksels Reimanis</h4>
                        <span class="age">&nbsp;</span>
                        <p>PIKC "Saldus tehnikums"</p>
                      </div>
                    </div>
                    <div class="app_content_subrow_column third-column">
                      <img src="images/noimage.jpg" alt="">
                      <div class="overlay"></div>
                      <div class="overlay-content">
                        <h4 class="name">Maksims Kačanovs</h4>
                        <span class="age">&nbsp;</span>
                        <p>PIKC "Daugavpils tehnikums"</p>
                      </div>
                    </div>
                    <div class="clearer"></div>
                  </div>

                  <div class="app_content_subrow_row">
                    <div class="app_content_subrow_column first-column">
                      <img src="images/noimage.jpg" alt="">
                        <div class="overlay"></div>
                        <div class="overlay-content">
                          <h4 class="name">Alberts Zemzāle</h4>
                          <span class="age">&nbsp;</span>
                          <p>PIKC "Rīgas Valsts tehnikums"</p>
                        </div>
                      </div>
                      <div class="app_content_subrow_column second-column">
                        <img src="images/noimage.jpg" alt="">
                        <div class="overlay"></div>
                        <div class="overlay-content">
                          <h4 class="name">Andis Lediņš</h4>
                          <span class="age">&nbsp;</span>
                          <p>PIKC "Jelgavas Tehnikums"</p>
                        </div>
                      </div>
                      <div class="app_content_subrow_column third-column">
                        <img src="images/noimage.jpg" alt="">
                        <div class="overlay"></div>
                        <div class="overlay-content">
                          <h4 class="name">Lauris Grosu</h4>
                          <span class="age">&nbsp;</span>
                          <p>Jēkabpils Agrobiznesa koledža </p>
                        </div>
                      </div>
                    <div class="clearer"></div>
                  </div>
                  <div class="app_content_subrow_row">
                    <div class="app_content_subrow_column first-column">
                      <img src="images/noimage.jpg" alt="">
                      <div class="overlay"></div>
                      <div class="overlay-content">
                        <h4 class="name">Edgars Indriķis</h4>
                        <span class="age">&nbsp;</span>
                        <p>PIKC "Rīgas Tehniskā koledža"</p>
                      </div>
                    </div>
                    <div class="clearer"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
-->



