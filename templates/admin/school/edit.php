<h2 class="sub-header"><?= $school->name; ?></h2>

<form action="<?= $this->urlFor('school_edit', array('id' => $school->id)) ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
  <div class="form-group">
    <label for="school_name" class="col-sm-2 control-label">Skolas nosaukums</label>
    <div class="col-sm-10">
        <input type="text" name="name" value="<?= $school->name ?>" class="form-control" id="school_name" placeholder="Skolas nosaukums">
    </div>
  </div>
  <div class="form-group">
    <label for="school_url" class="col-sm-2 control-label">Skolas lapas adrese</label>
    <div class="col-sm-10">
        <input type="text" name="url" value="<?= $school->url ?>" class="form-control" id="school_url" placeholder="Skolas lapas adrese">
    </div>
  </div>
  <div class="form-group">
    <label for="school_region" class="col-sm-2 control-label">Reģions</label>
    <div class="col-sm-10">
      <select name="region" id="school_region" class="form-control">
      <?php $school_region = $school->region()->first(); ?>
      <?php foreach ($regions as $region): ?>
        <?php if ($region->id == $school_region->id): ?>
        <option value="<?= $region->id; ?>" selected="selected"><?= $region->name; ?></option>
        <?php else: ?>
        <option value="<?= $region->id; ?>"><?= $region->name; ?></option>
        <?php endif ?>
      <?php endforeach ?>
      </select>
    </div>
  </div>
  <?php if (isset($errors['image']) && !empty($errors['image'])): ?>
    <div class="form-group has-error">
  <?php else: ?>
    <div class="form-group">
  <?php endif ?>
    <label for="school_image" class="col-sm-2 control-label">Foto</label>
    <div class="col-sm-10">
      <div class="cols-xs-12 col-sm-5">
        <?php $school_image = $school->image()->first(); ?>
        <?php if ($school_image): ?>
        <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="/images/<?= $school_image->filename; ?>">
        <?php else: ?>
        <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="https://dummyimage.com/280x190/d9d9d9/ebebeb.png">
        <?php endif ?>
      </div>
      <div class="cols-xs-12 col-sm-7">
        <input type="file" id="school_image" name="image">
      </div>
      <span class="help-block">
        <?= isset($errors['image']) ? $errors['image'][0] : '' ?>
      </span>
    </div>
  </div>
  <div class="pull-right">
    <a href="<?= $this->urlFor('school_list') ?>" class="btn btn-default">Atpakaļ</a>
    <button type="submit" class="btn btn-primary">Saglabat</button>
  </div>
</form>