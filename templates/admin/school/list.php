<h2 class="sub-header">Skolas
  <a class="btn btn-primary pull-right" href="<?= $this->urlFor('school_add') ?>" role="button">Pievienot</a>
</h2>

<table class="table table-striped">
<?php foreach ($schools as $school): ?>
  <tr>
    <td>
      <div class="row">
        <div class="col-xs-12 col-md-4">
          <?php $school_image = $school->image()->first(); ?>
          <?php if ($school_image): ?>
          <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="/images/<?= $school_image->filename; ?>">
          <?php else: ?>
          <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="https://dummyimage.com/1200x790/d9d9d9/ebebeb.png">
          <?php endif ?>
        </div>
        <div class="col-xs-6 col-md-8">
          <h3><?= $school->name ?></h3>
          <p><?= $school->region()->first()->name; ?>, Konkursanti: <?= $school->students()->count(); ?></p>
          <?php if (preg_match("/^http[s]*:\/\/[\w]+/i", $school->url, $match)): ?>
            <p>Vairāk par skolu: <a href="<?= $school->url; ?>" target="_blank"><?= $school->url ?></a></p>
          <?php else: ?>
            <p>Vairāk par skolu: <a href="//<?= $school->url; ?>" target="_blank"><?= $school->url ?></a></p>
          <?php endif ?>
          <p>
            <a class="btn btn-default btn-sm" href="<?= $this->urlFor('school', array('id' => $school->id)) ?>" role="button">Skatīt</a>
            <a class="btn btn-primary btn-sm" href="<?= $this->urlFor('school_edit', array('id' => $school->id)) ?>" role="button">Labot</a>
          </p>
        </div>
      </div>
    </td>
  </tr>
<?php endforeach ?>
</table>
