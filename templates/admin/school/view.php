<h2 class="sub-header"><?= $school->name ?></h2>

<div class="row">
  <div class="col-xs-12 col-md-4">
    <?php $school_image = $school->image()->first(); ?>
    <?php if ($school_image): ?>
    <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="/images/<?= $school_image->filename; ?>">
    <?php else: ?>
    <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="https://dummyimage.com/1200x790/d9d9d9/ebebeb.png">
    <?php endif ?>
  </div>
  <div class="col-xs-6 col-md-8">
    <p><?= $school->region()->first()->name; ?></p>
    <p>Konkursanti: <?= $school->students()->count(); ?></p>
    <?php if (preg_match("/^http[s]*:\/\/[\w]+/i", $school->url, $match)): ?>
      <p>Vairāk par skolu: <a href="<?= $school->url; ?>" target="_blank"><?= $school->url ?></a></p>
    <?php else: ?>
      <p>Vairāk par skolu: <a href="//<?= $school->url; ?>" target="_blank"><?= $school->url ?></a></p>
    <?php endif ?>
    <a class="btn btn-default btn-sm" href="<?= $this->urlFor('school_edit', array('id' => $school->id)) ?>" role="button">Labot</a>
  </div>
</div>

<h3 class="sub-header">Skolas studenti</h3>

<table class="table table-striped">
<?php foreach ($school->students()->get() as $student): ?>
  <tr>
    <td>
      <div class="row">
        <div class="col-xs-12 col-md-3">
          <?php $student_image = $student->image()->first(); ?>
          <?php if ($student_image): ?>
          <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="/images/<?= $student_image->filename; ?>">
          <?php else: ?>
          <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="https://dummyimage.com/380x380/d9d9d9/ebebeb.png">
          <?php endif ?>
        </div>
        <div class="col-xs-6 col-md-9">
          <h3><?= $student['name']; ?></h3>
          <?php if ($student->birthdate): ?>
            <?php $date = new DateTime($student->birthdate, new DateTimeZone('Europe/Riga')); ?>
            <p>Dzimšanas gads: <?= $date->format('Y') ?></p>
          <?php else: ?>
            <p>Dzimšanas gads: <span class="text-danger">nav uzstādīts</span></p>
          <?php endif ?>
          <p>Professija: <a href="<?= $this->urlFor('career', array('id' => $student->school()->first()->id)) ?>"><?= $student->career()->first()->name ?></a></p>
          <a class="btn btn-default btn-sm" href="<?= $this->urlFor('student', array('id' => $student->id)) ?>edit/" role="button">Labot</a>
        </div>
      </div>
    </td>
  </tr>
<?php endforeach ?>
</table>
