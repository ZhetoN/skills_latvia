<h2 class="sub-header">
    <?= $career->name; ?>
    <a class="btn btn-primary pull-right" href="<?= $this->urlFor('career_edit', array('id' => $career->id)) ?>" role="button">Labot</a>
</h2>

<?php $occupation = $career->occupation()->first(); ?>
<?php if ($occupation): ?>
<p>Nozare: <?= $career->occupation()->first()->name; ?></p>
<?php else: ?>
<p>Nozare: <span class="text-danger">nav uzstādīts</span></p>
<?php endif ?>
<p>Studentu skaits: <?= $career->students()->count(); ?></p>

<h3 class="sub-header">Profesijas studenti</h3>

<table class="table table-striped">
<?php foreach ($career->students()->get() as $student): ?>
  <tr>
    <td>
      <div class="row">
        <div class="col-xs-12 col-md-3">
          <?php $student_image = $student->image()->first(); ?>
          <?php if ($student_image): ?>
          <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="/images/<?= $student_image->filename; ?>">
          <?php else: ?>
          <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="https://dummyimage.com/380x380/d9d9d9/ebebeb.png">
          <?php endif ?>
        </div>
        <div class="col-xs-6 col-md-9">
          <h3><?= $student['name']; ?></h3>
          <?php if ($student->birthdate): ?>
            <?php $date = new DateTime($student->birthdate, new DateTimeZone('Europe/Riga')); ?>
            <p>Dzimšanas gads: <?= $date->format('Y') ?></p>
          <?php else: ?>
            <p>Dzimšanas gads: <span class="text-danger">nav uzstādīts</span></p>
          <?php endif ?>
          <p>Professija: <a href="<?= $this->urlFor('career', array('id' => $student->school()->first()->id)) ?>"><?= $student->career()->first()->name ?></a></p>
          <a class="btn btn-default btn-sm" href="<?= $this->urlFor('student', array('id' => $student->id)) ?>edit/" role="button">Labot</a>
        </div>
      </div>
    </td>
  </tr>
<?php endforeach ?>
</table>