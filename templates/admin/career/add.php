<h2 class="sub-header">Pievienot jaunu profesiju</h2>

<form action="<?= $this->urlFor('career_add') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
  <div class="form-group">
    <label for="career_name" class="col-sm-2 control-label">Profesijas nosaukums</label>
    <div class="col-sm-10">
        <input type="text" name="name" value="<?= $career->name ?>" class="form-control" id="career_name" placeholder="Profesijas nosaukums">
    </div>
  </div>
  <div class="form-group">
    <label for="career_occupation" class="col-sm-2 control-label">Nozare</label>
    <div class="col-sm-10">
      <select name="occupation" id="career_occupation" class="form-control">
        <option value="" selected="selected">nav uzstādīts</option>
        <?php foreach ($occupations as $occupation): ?>
        <option value="<?= $occupation->id; ?>"><?= $occupation->name; ?></option>
        <?php endforeach ?>
      </select>
    </div>
  </div>
  <div class="pull-right">
    <a href="<?= $this->urlFor('career_list') ?>" class="btn btn-default">Atpakaļ</a>
    <button type="submit" class="btn btn-primary">Saglabat</button>
  </div>
</form>
