<h2 class="sub-header">Profesijas
  <a class="btn btn-primary pull-right" href="<?= $this->urlFor('career_add') ?>" role="button">Pievienot</a>
</h2>

<table class="table table-striped">
<?php foreach ($careers as $career): ?>
  <tr>
    <td>
      <div class="row">
        <div class="col-xs-10 col-md-10">
          <h3><?= $career['name']; ?></h3>
          <?php $occupation = $career->occupation()->first(); ?>
          <?php if ($occupation): ?>
          <p>Nozare: <?= $career->occupation()->first()->name; ?></p>
          <?php else: ?>
          <p>Nozare: <span class="text-danger">nav uzstādīts</span></p>
          <?php endif ?>
          <p>Studentu skaits: <?= $career->students()->count(); ?></p>
        </div>
      </div>
    </td>
    <td>
      <div class="row">
        <h3>
          <a class="btn btn-default btn-sm" href="<?= $this->urlFor('career', array('id' => $career->id)) ?>" role="button">Skatīt</a>
          <a class="btn btn-primary btn-sm" href="<?= $this->urlFor('career_edit', array('id' => $career->id)) ?>" role="button">Labot</a>
        </h3>
      </div>
    </td>
  </tr>
<?php endforeach ?>
</table>
