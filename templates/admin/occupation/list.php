<h2 class="sub-header">Nozares
  <a class="btn btn-primary pull-right" href="<?= $this->urlFor('occupation_add') ?>" role="button">Pievienot</a>
</h2>

<table class="table table-striped">
<?php foreach ($occupations as $occupation): ?>
  <tr>
    <td>
      <div class="row">
        <div class="col-xs-10 col-md-9">
          <h3><?= $occupation['name']; ?></h3>
        </div>
      </div>
    </td>
    <td>
      <div class="row">
        <h3>
          <a class="btn btn-default btn-sm" href="<?= $this->urlFor('occupation', array('id' => $occupation->id)) ?>" role="button">Skatīt</a>
          <a class="btn btn-primary btn-sm" href="<?= $this->urlFor('occupation_edit', array('id' => $occupation->id)) ?>" role="button">Labot</a>
        </h3>
      </div>
    </td>
  </tr>
<?php endforeach ?>
</table>
