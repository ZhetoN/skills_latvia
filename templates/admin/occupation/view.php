<h2 class="sub-header">
  <?= $occupation['name']; ?>
  <a class="btn btn-primary pull-right" href="<?= $this->urlFor('occupation_edit', array('id' => $occupation->id)) ?>" role="button">Labot</a>
</h2>

<h3 class="sub-header">Nozares profesijas</h3>

<table class="table table-striped">
<?php foreach ($occupation->careers()->get() as $career): ?>
  <tr>
    <td>
      <div class="row">
        <div class="col-xs-10 col-md-10">
          <h3><?= $career['name']; ?></h3>
          <p>Studentu skaits: <?= $career->students()->count(); ?></p>
        </div>
      </div>
    </td>
    <td>
      <div class="row">
        <div class="col-xs-2 col-md-2">
          <h3><a class="btn btn-default btn-sm" href="<?= $this->urlFor('career', array('id' => $career->id)) ?>" role="button">Skatīt</a></h3>
        </div>
      </div>
    </td>
  </tr>
<?php endforeach ?>
</table>

