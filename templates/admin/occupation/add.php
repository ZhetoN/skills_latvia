<h2 class="sub-header">Pievienot jaunu nozari</h2>

<form action="<?= $this->urlFor('occupation_add') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
  <div class="form-group">
    <label for="occupation_name" class="col-sm-2 control-label">Nozares nosaukums</label>
    <div class="col-sm-10">
        <input type="text" name="name" value="<?= $occupation->name ?>" class="form-control" id="occupation_name" placeholder="Nozares nosaukums">
    </div>
  </div>
  <div class="pull-right">
    <a href="<?= $this->urlFor('occupation_list') ?>" class="btn btn-default">Atpakaļ</a>
    <button type="submit" class="btn btn-primary">Saglabat</button>
  </div>
</form>
