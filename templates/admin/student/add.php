<h2 class="sub-header">Pievienot studentu</h2>

<form action="<?= $this->urlFor('student_add') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
  <div class="form-group">
    <label for="student_name" class="col-sm-2 control-label">Vārds, Uzvārds</label>
    <div class="col-sm-10">
        <input type="text" name="name" value="<?= $student->name; ?>" class="form-control" id="student_name" placeholder="Vārds, Uzvārds">
    </div>
  </div>
  <div class="form-group">
    <label for="student_birthdate" class="col-sm-2 control-label">Dzimšanas gads</label>
    <div class="col-sm-10">
      <?php
        $birthdate = new DateTime($student->birthdate, new DateTimeZone('Europe/Riga'));
        $birthdate = $birthdate->format('Y');
        $first_year = new DateTime();
        $first_year = $first_year->modify('-30 year')->format('Y');
        $last_year = new DateTime();
        $last_year = $last_year->modify('-10 year')->format('Y');
      ?>
      <select name="birthdate" id="student_birthdate" class="form-control">
        <option value="">nav uzstādīts</option>
        <?php for ($year = $first_year; $year < $last_year; $year++): ?>
          <?php $date = new DateTime("{$year}-01-01", new DateTimeZone('Europe/Riga')); ?>
          <?php if ($birthdate == $year): ?>
            <option value="<?= $date->format('Y-m-d H:i:s') ?>" selected="selected"><?= $year ?></option>
          <?php else: ?>
            <option value="<?= $date->format('Y-m-d H:i:s') ?>"><?= $year ?></option>
          <?php endif ?>
        <?php endfor ?>
      </select>
    </div>
  </div>
  <div class="form-group<?= isset($errors['school']) ? ' has-error' : '' ?>">
    <label for="student_school" class="col-sm-2 control-label">Skola</label>
    <div class="col-sm-10">
      <select name="school" id="student_school" class="form-control">
        <option value="">nav uzstādīts</option>
        <?php foreach ($schools as $school): ?>
          <?php if ($school->id == $student->school): ?>
          <option value="<?= $school->id; ?>" selected="selected"><?= $school->name; ?></option>
          <?php else: ?>
          <option value="<?= $school->id; ?>"><?= $school->name; ?></option>
          <?php endif ?>
        <?php endforeach ?>
      </select>
      <span class="help-block">
        <?= isset($errors['school']) ? $errors['school'] : '' ?>
      </span>
    </div>
  </div>
  <div class="form-group<?= isset($errors['career']) ? ' has-error' : '' ?>">
    <label for="student_career" class="col-sm-2 control-label">Profesija</label>
    <div class="col-sm-10">
      <select name="career" id="student_career" class="form-control">
        <option value="">nav uzstādīts</option>
        <?php foreach ($careers as $career): ?>
          <?php if ($career->id == $student->career): ?>
          <option value="<?= $career->id; ?>" selected="selected"><?= $career->name; ?></option>
          <?php else: ?>
          <option value="<?= $career->id; ?>"><?= $career->name; ?></option>
          <?php endif ?>
        <?php endforeach ?>
      </select>
      <span class="help-block">
        <?= isset($errors['career']) ? $errors['career'] : '' ?>
      </span>
    </div>
  </div>
  <div class="form-group">
    <label for="student_image" class="col-sm-2 control-label">Foto</label>
    <div class="col-sm-10">
      <div class="cols-xs-12 col-sm-4">
        <?php if ($student->image): ?>
        <input type="hidden" name="image" value="<?= $student->image; ?>" />
        <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="/images/<?= $student->image()->first()->filename; ?>">
        <?php else: ?>
        <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="https://dummyimage.com/380x380/d9d9d9/ebebeb.png">
        <?php endif ?>
      </div>
      <div class="cols-xs-12 col-sm-8">
        <input type="file" id="student_image" name="image">
      </div>
    </div>
    <span class="help-block">
      <?= isset($errors['career']) ? $errors['career'][0] : '' ?>
    </span>
  </div>
  <div class="pull-right">
    <a href="<?= $this->urlFor('student_list') ?>" class="btn btn-default">Atpakaļ</a>
    <button type="submit" class="btn btn-primary">Saglabat</button>
  </div>
</form>
