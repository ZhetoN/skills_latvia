<h2 class="sub-header"><?= $student->name ?></h2>

<div class="row">
  <div class="col-xs-12 col-md-3">
    <?php $student_image = $student->image()->first(); ?>
    <?php if ($student_image): ?>
    <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="/images/<?= $student_image->filename; ?>">
    <?php else: ?>
    <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="https://dummyimage.com/380x380/d9d9d9/ebebeb.png">
    <?php endif ?>
  </div>
  <div class="col-xs-6 col-md-9">
    <?php if ($student->birthdate): ?>
      <?php $date = new DateTime($student->birthdate, new DateTimeZone('Europe/Riga')); ?>
      <p>Dzimšanas gads: <?= $date->format('Y') ?></p>
    <?php else: ?>
      <p>Dzimšanas gads: <span class="text-danger">nav uzstādīts</span></p>
    <?php endif ?>
    <p>Skola: <a href="<?= $this->urlFor('school', array('id' => $student->school()->first()->id)) ?>"><?= $student->school()->first()->name; ?></a></p>
    <p>Professija: <a href="<?= $this->urlFor('career', array('id' => $student->school()->first()->id)) ?>"><?= $student->career()->first()->name ?></a></p>
    <p>
      <a class="btn btn-default btn-sm" href="<?= $this->urlFor('student_edit', array('id' => $student->id)) ?>" role="button">Labot</a>
    </p>
  </div>
</div>