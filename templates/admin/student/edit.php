<h2 class="sub-header"><?= $student->name; ?></h2>

<form action="<?= $this->urlFor('student', array('id' => $student->id)) ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
  <div class="form-group">
    <label for="student_name" class="col-sm-2 control-label">Vārds, Uzvārds</label>
    <div class="col-sm-10">
        <input type="text" name="name" value="<?= $student->name; ?>" class="form-control" id="student_name" placeholder="Vārds, Uzvārds">
    </div>
  </div>
  <div class="form-group">
    <label for="student_birthdate" class="col-sm-2 control-label">Dzimšanas gads</label>
    <div class="col-sm-10">
      <?php
        $birthdate = new DateTime($student->birthdate, new DateTimeZone('Europe/Riga'));
        $birthdate = $birthdate->format('Y');
        $first_year = new DateTime();
        $first_year = $first_year->modify('-30 year')->format('Y');
        $last_year = new DateTime();
        $last_year = $last_year->modify('-10 year')->format('Y');
      ?>
      <select name="birthdate" id="student_birthdate" class="form-control">
        <option value="">nav uzstādīts</option>
        <?php for ($year = $first_year; $year < $last_year; $year++): ?>
          <?php $date = new DateTime("{$year}-01-01", new DateTimeZone('Europe/Riga')); ?>
          <?php if ($birthdate == $year): ?>
            <option value="<?= $date->format('Y-m-d H:i:s') ?>" selected="selected"><?= $year ?></option>
          <?php else: ?>
            <option value="<?= $date->format('Y-m-d H:i:s') ?>"><?= $year ?></option>
          <?php endif ?>
        <?php endfor ?>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="student_school" class="col-sm-2 control-label">Skola</label>
    <div class="col-sm-10">
      <select name="school" id="student_school" class="form-control">
        <?php $student_school = $student->school()->first(); ?>
        <?php foreach ($schools as $school): ?>
          <?php if ($school->id == $student_school->id): ?>
          <option value="<?= $school->id; ?>" selected="selected"><?= $school->name; ?></option>
          <?php else: ?>
          <option value="<?= $school->id; ?>"><?= $school->name; ?></option>
          <?php endif ?>
        <?php endforeach ?>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="student_career" class="col-sm-2 control-label">Profesija</label>
    <div class="col-sm-10">
      <select name="career" id="student_career" class="form-control">
        <?php $student_career = $student->career()->first(); ?>
        <?php foreach ($careers as $career): ?>
          <?php if ($career->id == $student_career->id): ?>
          <option value="<?= $career->id; ?>" selected="selected"><?= $career->name; ?></option>
          <?php else: ?>
          <option value="<?= $career->id; ?>"><?= $career->name; ?></option>
          <?php endif ?>
        <?php endforeach ?>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="student_image" class="col-sm-2 control-label">Foto</label>
    <div class="col-sm-10">
      <div class="cols-xs-12 col-sm-4">
        <?php $student_image = $student->image()->first(); ?>
        <?php if ($student_image): ?>
        <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="/images/<?= $student_image->filename; ?>">
        <?php else: ?>
        <img class="img-thumbnail img-responsive" data-holder-rendered="true" src="https://dummyimage.com/380x380/d9d9d9/ebebeb.png">
        <?php endif ?>
      </div>
      <div class="cols-xs-12 col-sm-8">
        <input type="file" id="student_image" name="image">
      </div>
    </div>
  </div>
  <div class="pull-right">
    <a href="<?= $this->urlFor('student_list') ?>" class="btn btn-default">Atpakaļ</a>
    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Dzest</button>
    <button type="submit" class="btn btn-primary">Saglabat</button>
  </div>
</form>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Dzēst studentu</h4>
      </div>
      <div class="modal-body">
        <p>Students <?= $student->name ?> tiks dzēsts.</p>
        <p><strong class="text-danger">Darbība ir neatgriezeniska.</strong></p>
      </div>
      <div class="modal-footer">
        <form action="<?= $this->urlFor('student_delete', array('id' => $student->id)) ?>" method="post">
           <a href="#" class="btn btn-default" data-dismiss="modal">Atcelt</a>
           <button type="submit" class="btn btn-danger">Dzest</button>
        </form>
      </div>
    </div>
  </div>
</div>
