<?php

require_once realpath(__DIR__ . '/..') . '/vendor/autoload.php';
$config = require_once realpath(__DIR__ . '/..') . '/config.php';

define('BASE_DIR', realpath(__DIR__));

use Illuminate\Database\Capsule\Manager as Capsule;
use App\models\School;
use App\models\Student;
use App\models\Occupation;
use App\models\Career;
use App\models\Region;

$capsule = new Capsule;

$capsule->addConnection(array(
    'driver'    => 'mysql',
    'host'      => $config['host'],
    'database'  => $config['database'],
    'username'  => $config['username'],
    'password'  => $config['password'],
    'charset'   => 'utf8mb4',
    'collation' => 'utf8mb4_general_ci',
    'prefix'    => 'skills_',
));

$capsule->setAsGlobal();
$capsule->bootEloquent();

\Slim\Route::setDefaultConditions(array(
    'id' => '\d+'
));

$app = new \Slim\Slim(array(
    'view' => new App\AppView(),
    'templates.path' => '../templates/admin'
));

$app->get('/', function() {
    $frontend = new \Slim\Slim(array(
        'templates.path' => '../templates/frontend'
    ));
    $regions = Region::with('schools')->get();
    $occupations = Occupation::with(array(
        'careers',
        'careers.students'
    ))->get();

    return $frontend->render(
        'main.php',
        array(
            'regions' => $regions,
            'occupations' => $occupations
        )
    );
});

$app->group('/admin', function () use ($app) {

    $app->get('/', function() use ($app) {
        return $app->response->redirect(
            $app->urlFor('school_list')
        );
    });

    // schools
    $app->get('/school(/)', function() use ($app) {
        $schools = School::all();
        return $app->render('school/list.php', array('schools' => $schools));
    })->name('school_list');

    $app->map('/school/add(/)', function() use ($app) {
        $errors = array();
        $school = new School(
            array(
                'region' => null,
                'image' => null
            )
        );

        if ($app->request->isPost()) {
            $data = $app->request->post();

            if (empty($data['region'])) {
                $errors['region'] = 'Lūdzu izvēlēties reģionu';
            }

            if(file_exists($_FILES['image']['tmp_name'])
                || is_uploaded_file($_FILES['image']['tmp_name'])
            ) {
                $storage = new \Upload\Storage\FileSystem(
                    BASE_DIR . '/images'
                );

                $file = new \Upload\File('image', $storage);

                $original_name = $file->getNameWithExtension();
                $new_filename = uniqid();
                $file->setName($new_filename);

                $file->addValidations(array(
                    new \Upload\Validation\Mimetype(
                        array('image/jpg', 'image/jpeg', 'image/png')
                    ),
                    new \Upload\Validation\Size('15M')
                ));

                $image = new App\models\Image;

                $image->mime = $file->getMimetype();
                $image->filename = $file->getNameWithExtension();
                $image->name = $original_name;

                try {
                    $file->upload();
                    $image->save();
                    $data['image'] = $image->id;
                } catch (\Exception $e) {
                    $errors['image'] = $file->getErrors();
                }
            }

            $school->fill($data);

            if (empty($errors)) {
                $school->save();
                return $app->response->redirect(
                    $app->urlFor('school', array('id' => $school->id))
                );
            }
        }

        $regions = Region::all();
        return $app->render(
            'school/add.php',
            array(
                'school' => $school,
                'regions' => $regions,
                'errors' => $errors
            )
        );
    })->via('GET', 'POST')->name('school_add');

    $app->get('/school/:id(/)', function($id) use ($app) {
        $school = School::find($id);
        if (empty($school)) {
            $app->notFound();
        }
        return $app->render('school/view.php', array('school' => $school));
    })->name('school');

    $app->map('/school/:id/edit(/)', function($id) use ($app) {
        $errors = array();
        $school = School::find($id);
        if (empty($school)) {
            $app->notFound();
        }

        if ($app->request->isPost()) {
            $data = $app->request->post();

            if (empty($data['region'])) {
                $errors['region'] = 'Lūdzu izvēlēties reģionu';
            }

            if(file_exists($_FILES['image']['tmp_name'])
                || is_uploaded_file($_FILES['image']['tmp_name'])
            ) {
                $storage = new \Upload\Storage\FileSystem(
                    BASE_DIR . '/images'
                );

                $file = new \Upload\File('image', $storage);

                $original_name = $file->getNameWithExtension();
                $new_filename = uniqid();
                $file->setName($new_filename);

                $file->addValidations(array(
                    new \Upload\Validation\Mimetype(
                        array('image/jpg', 'image/jpeg', 'image/png')
                    ),
                    new \Upload\Validation\Size('15M')
                ));

                $image = new App\models\Image;

                $image->mime = $file->getMimetype();
                $image->filename = $file->getNameWithExtension();
                $image->name = $original_name;

                try {
                    $file->upload();
                    $image->save();
                    $data['image'] = $image->id;
                } catch (\Exception $e) {
                    $errors['image'] = $file->getErrors();
                }
            }

            $school->fill($data);

            if (empty($errors)) {
                $school->save();
                return $app->response->redirect(
                    $app->urlFor('school', array('id' => $school->id))
                );
            }
        }

        $regions = Region::all();
        return $app->render(
            'school/edit.php',
            array(
                'school' => $school,
                'regions' => $regions,
                'errors' => $errors
            )
        );
    })->via('GET', 'POST')->name('school_edit');

    // students
    $app->get('/student(/)', function() use ($app) {
        $students = Student::with(array('school', 'career'))->get();
        return $app->render(
            'student/list.php',
            array('students' => $students)
        );
    })->name('student_list');

    $app->post('/student/:id/delete(/)', function($id) use ($app) {
        $student = Student::find($id);
        if (empty($student)) {
            $app->notFound();
        }

        $image = $student->image()->first();
        $student->delete();
        if ($image) {
            $path = BASE_DIR . '/images/' . $image->filename;
            if (file_exists($path)) {
                unlink($path);
            }
            $student->image()->delete();
        }
        return $app->response->redirect(
            $app->urlFor('student_list')
        );
    })->name('student_delete');

    $app->map('/student/add(/)', function() use ($app) {
        $errors = array();
        $student = new Student(
            array(
                'school' => null,
                'career' => null,
                'image' => null
            )
        );
        $schools = School::all();
        $careers = Career::all();

        if ($app->request->isPost()) {
            $data = $app->request->post();

            if (!empty($data['birthdate'])) {
                $date = DateTime::createFromFormat(
                    'Y-m-d G:i:s',
                    $data['birthdate']
                );
                if ($date === false) {
                    $errors['birthdate'] = "Datumu formāts ir nepareizs";
                }
            } else {
                unset($data['birthdate']);
            }

            if (empty($data['school'])) {
                $errors['school'] = 'Lūdzu izvēlēties skolu';
            }
            if (empty($data['career'])) {
                $errors['career'] = 'Lūdzu izvēlēties profesiju';
            }

            if(file_exists($_FILES['image']['tmp_name'])
                || is_uploaded_file($_FILES['image']['tmp_name'])
            ) {
                $storage = new \Upload\Storage\FileSystem(
                    BASE_DIR . '/images'
                );

                $file = new \Upload\File('image', $storage);

                $original_name = $file->getNameWithExtension();
                $new_filename = uniqid();
                $file->setName($new_filename);

                $file->addValidations(array(
                    new \Upload\Validation\Mimetype(
                        array('image/jpg', 'image/jpeg', 'image/png')
                    ),
                    new \Upload\Validation\Size('15M')
                ));

                $image = new App\models\Image;

                $image->mime = $file->getMimetype();
                $image->filename = $file->getNameWithExtension();
                $image->name = $original_name;

                try {
                    $file->upload();
                    $image->save();
                    $data['image'] = $image->id;
                } catch (\Exception $e) {
                    $errors['image'] = $file->getErrors();
                }
            }

            $student->fill($data);

            if (empty($errors)) {
                $student->save();
                return $app->response->redirect(
                    $app->urlFor('student', array('id' => $student->id))
                );
            }
        }

        return $app->render(
            'student/add.php',
            array(
                'student' => $student,
                'schools' => $schools,
                'careers' => $careers,
                'errors' => $errors
            )
        );
    })->via('GET', 'POST')->name('student_add');

    $app->map('/student/:id(/)', function($id) use ($app) {
        $student = Student::find($id);
        if (empty($student)) {
            $app->notFound();
        }
        if ($app->request->isPost()) {
            $data = $app->request->post();
            $errors = array();

            if (!empty($data['birthdate'])) {
                $date = DateTime::createFromFormat(
                    'Y-m-d G:i:s',
                    $data['birthdate']
                );
                if ($date === false) {
                    $errors['birthdate'] = "Datumu formāts ir nepareizs";
                }
            } else {
                unset($data['birthdate']);
            }

            if(file_exists($_FILES['image']['tmp_name'])
                || is_uploaded_file($_FILES['image']['tmp_name'])
            ) {
                $storage = new \Upload\Storage\FileSystem(
                    BASE_DIR . '/images'
                );

                $file = new \Upload\File('image', $storage);

                $original_name = $file->getNameWithExtension();
                $new_filename = uniqid();
                $file->setName($new_filename);

                $file->addValidations(array(
                    new \Upload\Validation\Mimetype(
                        array('image/jpg', 'image/jpeg', 'image/png')
                    ),
                    new \Upload\Validation\Size('15M')
                ));

                $image = new App\models\Image;

                $image->mime = $file->getMimetype();
                $image->filename = $file->getNameWithExtension();
                $image->name = $original_name;

                try {
                    $file->upload();
                    $image->save();
                    $data['image'] = $image->id;
                } catch (\Exception $e) {
                    $errors['image'] = $file->getErrors();
                    return $app->render(
                        'student/edit.php',
                        array(
                            'school' => $school,
                            'errors' => $errors,
                        )
                    );
                }
            }

            if (empty($errors)) {
                $student->fill($data);
                $student->save();
            } else {
                $app->response->redirect(
                    $app->urlFor('student_edit', array('id' => $id))
                );
            }
        }
        return $app->render(
            'student/view.php',
            array('student' => $student)
        );
    })->via('GET', 'POST')->name('student');

    $app->get('/student/:id/edit(/)', function($id) use ($app) {
        $student = Student::find($id);
        if (empty($student)) {
            $app->notFound();
        }
        $schools = School::all();
        $careers = Career::all();
        return $app->render(
            'student/edit.php',
            array(
                'student' => $student,
                'schools' => $schools,
                'careers' => $careers,
            )
        );
    })->name('student_edit');

    // occupations
    $app->get('/occupation(/)', function() use ($app) {
        $occupations = Occupation::all();
        return $app->render(
            'occupation/list.php',
            array('occupations' => $occupations)
        );
    })->name('occupation_list');

    $app->map('/occupation/add(/)', function() use ($app) {
        $occupation = new Occupation;
        if ($app->request->isPost()) {
            $occupation->fill($app->request->post());
            $occupation->save();
            $app->response->redirect(
                $app->urlFor('occupation', array('id' => $occupation->id))
            );
        }
        return $app->render(
            'occupation/add.php',
            array('occupation' => $occupation)
        );
    })->via('GET', 'POST')->name('occupation_add');

    $app->map('/occupation/:id(/)', function($id) use ($app) {
        $occupation = Occupation::find($id);
        if (empty($occupation)) {
            $app->notFound();
        }
        return $app->render(
            'occupation/view.php',
            array('occupation' => $occupation)
        );
    })->via('GET', 'POST')->name('occupation');

    $app->get('/occupation/:id/edit(/)', function($id) use ($app) {
        $occupation = Occupation::find($id);
        if (empty($occupation)) {
            $app->notFound();
        }
        if ($app->request->isPost()) {
            $occupation->fill($app->request->post());
            $occupation->save();
            $app->response->redirect(
                $app->urlFor('occupation', array('id' => $id))
            );
        }
        return $app->render(
            'occupation/edit.php',
            array('occupation' => $occupation)
        );
    })->via('GET', 'POST')->name('occupation_edit');

    // careers
    $app->get('/career(/)', function() use ($app) {
        $careers = Career::all();
        return $app->render('career/list.php', array('careers' => $careers));
    })->name('career_list');

    $app->map('/career/add(/)', function() use ($app) {
        $career = new Career;
        if ($app->request->isPost()) {
            $career->fill($app->request->post());
            $career->save();
            $app->response->redirect(
                $app->urlFor('career', array('id' => $career->id))
            );
        }

        $occupations = Occupation::all();
        return $app->render(
            'career/add.php',
            array(
                'career' => $career,
                'occupations' => $occupations
            )
        );
    })->via('GET', 'POST')->name('career_add');

    $app->map('/career/:id(/)', function($id) use ($app) {
        $career = Career::find($id);
        if (empty($career)) {
            $app->notFound();
        }
        return $app->render('career/view.php', array('career' => $career));
    })->via('GET', 'POST')->name('career');

    $app->map('/career/:id/edit(/)', function($id) use ($app) {
        $career = Career::find($id);
        if (empty($career)) {
            $app->notFound();
        }
        if ($app->request->isPost()) {
            $data = $app->request->post();
            $career->fill($data);
            if (empty($data['occupation'])) {
                $career->occupation = null;
            }
            $career->save();
            $app->response->redirect(
                $app->urlFor('career', array('id' => $id))
            );
        }
        $occupations = Occupation::all();
        return $app->render(
            'career/edit.php',
            array(
                'career' => $career,
                'occupations' => $occupations
            )
        );
    })->via('GET', 'POST')->name('career_edit');
});

$app->run();
