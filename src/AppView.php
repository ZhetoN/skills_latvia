<?php

namespace App;

use \Slim;

class AppView extends \Slim\View
{
    public function render($template)
    {
        return implode(array(
            parent::render('header.php'),
            parent::render($template),
            parent::render('footer.php')
        ));
    }

    public function urlFor($name, $params = array(), $appName = 'default')
    {
        return \Slim\Slim::getInstance($appName)->urlFor($name, $params);
    }

    public function getCurrentRoute()
    {
        $app = new \Slim\Slim();
        return trim($app->request->getResourceUri(), '/');
    }
}
