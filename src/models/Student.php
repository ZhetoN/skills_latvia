<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Student extends Eloquent
{
   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $table = 'student';

  /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = array(
        'name',
        'school',
        'career',
        'birthdate',
        'image',
    );

  /**
    * Get School of User
    *
    */
    public function school()
    {
        return $this->belongsTo('App\models\School', 'school', 'id');
    }

  /**
    * Get Career of User
    *
    */
    public function career()
    {
        return $this->belongsTo('App\models\Career', 'career', 'id');
    }

  /**
    * Get Student image
    *
    */
    public function image()
    {
       return $this->hasOne('App\models\Image', 'id', 'image');
    }
}
