<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class School extends Eloquent
{
  /**
    * Table name.
    *
    * @var array
    */
    protected $table = 'school';

  /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = array(
        'name',
        'url',
        'image',
        'region',
    );

  /**
    * Get School of User
    *
    */
    public function students()
    {
       return $this->hasMany('App\models\Student', 'school', 'id');
    }

  /**
    * Get School Region
    *
    */
    public function region()
    {
       return $this->hasOne('App\models\Region', 'id', 'region');
    }

  /**
    * Get School image
    *
    */
    public function image()
    {
       return $this->hasOne('App\models\Image', 'id', 'image');
    }
}
