<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Occupation extends Eloquent
{
  /**
    * Table name.
    *
    * @var string
    */
    protected $table = 'occupation';

  /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = array('name');

  /**
    * Get Occupation Careers
    *
    */
    public function careers()
    {
       return $this->hasMany('App\models\Career', 'occupation', 'id');
    }

  /**
    * Get Occupation Students
    *
    */
    public function students()
    {
       return $this->hasManyThrough('App\models\Student', 'App\models\Career', 'occupation', 'id');
    }

  /**
    * Get Occupation icon
    *
    */
    public function icon()
    {
       return $this->hasOne('App\models\Image', 'id', 'icon');
    }
}
