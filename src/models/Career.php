<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Career extends Eloquent
{
  /**
    * Table name.
    *
    * @var string
    */
    protected $table = 'career';

  /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = array('name', 'occupation');

  /**
    * Get School of User
    *
    */
    public function students()
    {
       return $this->hasMany('App\models\Student', 'career', 'id');
    }

  /**
    * Get Career occupation
    *
    */
    public function occupation()
    {
       return $this->hasOne('App\models\Occupation', 'id', 'occupation');
    }
}
