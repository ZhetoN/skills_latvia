<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Image extends Eloquent
{
  /**
    * Table name.
    *
    * @var string
    */
    protected $table = 'image';

  /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = array('mime', 'name', 'filename');
}
