<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Region extends Eloquent
{
  /**
    * Table name.
    *
    * @var array
    */
    protected $table = 'region';

  /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = array('name');

  /**
    * Get School of User
    *
    */
    public function schools()
    {
       return $this->hasMany('App\models\School', 'region', 'id');
    }
}
